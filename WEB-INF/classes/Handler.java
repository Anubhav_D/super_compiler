import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.util.*;
import java.io.*;
@WebServlet("/run")
public class Handler extends HttpServlet {
	public static String file="Main";
	public static String filename="Main.java";
	public static String path="C:\\Users\\Anubhav\\Desktop\\apache-tomcat-9.0.0.M21\\webapps\\Super_Compiler\\UserCode\\";
	public static String file_path=path+filename;
	public static String input_filename="input.txt";
	public static String input_file_path=path+input_filename;
	public static String error_message=" ";
	public static String output=" ";
	public static boolean running=false;
	public static long max_time;
	public static final Object lock=new Object();

	public void doPost(HttpServletRequest req,HttpServletResponse res)
	{
		synchronized(lock){
		System.out.println("running under synchronized block");
		String code=req.getParameter("code");
		String input=req.getParameter("input");

		//--Writing UserCode onto Main.java 
		try(BufferedWriter bw=new BufferedWriter(new FileWriter(file_path))){
			bw.write(code);
		}
		catch(Exception e){
			e.printStackTrace();
		}

		//--Writing Input file
		try(BufferedWriter bw=new BufferedWriter(new FileWriter(input_file_path))){
			bw.write(input);
		}
		catch(Exception e){
			e.printStackTrace();
		}


		if(javac()){
			req.setAttribute("output",error_message);
		}
		else{
			System.out.println("compiled");
			if(java()){
				req.setAttribute("output",error_message);
			}
			else{
				req.setAttribute("output",output);
			}
			
		}
	
		req.setAttribute("code",code);
		req.setAttribute("input",input);
		
		try{
		req.getRequestDispatcher("Output.jsp").forward(req,res);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		}
	
	}

	public static boolean javac(){
		try{
			max_time=System.currentTimeMillis()+10000;
			boolean flag=false;
			Process p=Runtime.getRuntime().exec("javac "+file_path);
			System.out.println("Waiting");
			p.waitFor();
			System.out.println("Wait Over");
			BufferedReader br=new BufferedReader(new InputStreamReader(p.getErrorStream()));
			String temp;
			error_message="";
			while((temp=br.readLine())!=null){
				error_message+=temp;
				flag=true;
			}
			return flag;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public static boolean java(){
		try{
			boolean flag=false;
			Process p=Runtime.getRuntime().exec("java -cp "+path+" "+file);
			try(BufferedReader br=new BufferedReader(new FileReader(input_file_path))){
				while(true){
					if(System.currentTimeMillis()>=max_time){
						flag=true;
						error_message="Time Limit Exceeded:";
						break;
					}
					if(!p.isAlive())break;
					BufferedWriter bw=new BufferedWriter(new OutputStreamWriter(p.getOutputStream()));
					String temp;
					if((temp=br.readLine())!=null){
					bw.write(temp);
					bw.newLine();
					bw.flush();
					}
					Thread t=Thread.currentThread();
					t.sleep(100);
				}
			}
			catch(IOException e){
				e.printStackTrace();
			}
			System.out.println("half running done!");
			if(!flag){
			try(BufferedReader br=new BufferedReader(new InputStreamReader(p.getErrorStream()))){
				String temp;
				error_message="";
				while((temp=br.readLine())!=null){
					error_message+=temp;
					flag=true;
				}
			}
			catch(IOException e){
				e.printStackTrace();
			}
			try(BufferedReader br=new BufferedReader(new InputStreamReader(p.getInputStream()))){
				String temp;
				output="";
				while((temp=br.readLine())!=null){
					output+=temp;
				}
			}
			catch(IOException e){
				e.printStackTrace();
			}
		}
			return flag;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}
}