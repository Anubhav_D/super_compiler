<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1" /> 
<link href="usable.css" rel="Stylesheet" type="text/css" />
</head>
<body style="background-color: #edf7f0;">
<div class="row">
<div class="col-12">
<center>
<h1 style="font-family:Courier New;color:#59f442;">Super-Compiler</h1>
</center>
</div>
</div>
<div class="row">
<div class="col-12">
<center>
<h4 style="font-family:Courier New;">Compile java code from any browser and get sharable links</h4>
</center>
</div>
</div>
<hr style="color:gray" />
<%
String output=(String)request.getAttribute("output");
String code=(String)request.getAttribute("code");
String input=(String)request.getAttribute("input");
if(output==null)output="Nothing to show";
if(code==null)code="Nothing to show";
if(input==null)input="Nothing to show";
%>
<div class="row">
<div class="col-4">
<div class="row">
<div style="font-family: Courier;">Output</div>
</div>
<div class="row">
<textarea readonly rows="9" style="width:100%;border-color: 1px solid gray;border-radius:5px;background-color: White;padding: 3px;text-overflow:scroll;">
<%= output %>
</textarea>
</div>
<div class="row">
<div style="font-family: Courier;margin-top: 4px;">Input</div>
</div>
<div class="row">
<textarea readonly rows="9" style="width:100%;border-color: 1px solid gray;border-radius:5px;background-color: gray;padding: 3px;text-overflow:scroll;">
<%= input %>
</textarea>
</div>
</div>
<div class="line-space-5"></div>
<div class="col-8">
<div class="row">
<div style="font-family: Courier;">
Your Code:
</div>
</div>
<div class="row">
<textarea readonly rows="20" style="width:100%;border-color:1px solid gray;border-radius:5px;background-color: gray;color:black;padding: 3px;text-overflow: scroll;">
<%= code %>
</textarea>
</div>
</div>
</div>

</body>
</html>